import sys

"""
this is binary transform fuction
input = decimal some
ouput = string binary bi

"""
r=[0]*16
r[12]=2
r[7]=-1

def binaryPrint(some):
	A=[0]*32
	list=[0]
	for i in range(32):
        	reg=some%2
		if some/2==0:
         		break
        	some=some/2
        	A[31-i]=reg

	bi=""
	for j in range(32):
        	bi=bi+str(A[j])
	return bi
"""
this is MOV operand
input is binary String

"""
def unsigned32(n):
	return n & 0xFFFFFFFFL
	

def C_ROR(string):
	one=int(string[24:],2)
	shift=int(string[20:24],2)
	shift=shift*2
	tail=one>>shift
	print tail
	head=(one-(tail<<shift))<<(32-shift)
	return head+tail			#return value is int
	
def LSL(string):
	a=int(string[28:],2)			# string consist of binary number		
	one=r[a]				
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift
	else:
		temp=int(string[20:24],2)	# this case is register number shift
		shift=r[temp]	

	one=one<<shift				# excute shift left
	return one				# return value is shifted int

def LSR(string):				# string consist of binary number
	a=int(string[28:],2)
	one=r[a]
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift     
	else:
		temp=int(string[20:24],2)	# this case is register number shift	
		shift=r[temp]
	one=one>>shift				# excute shift right
	return one				# return value is int 

#this ASR function is MSB right shifter

def ASR(string):
	a=int(string[28:],2)
	one=r[a]
	uone=unsigned32
	if string[27]=="0":
		shift=int(string[20:25],2)
	else:
		temp=int(string[20:24],2)
		shift=r[temp]
	if unsigned32(one)>=21474836:		# call unsigned32 function if -1 inserted, MSB 1 ~~~
		temp2="11111111111111111111111111111111" # this number is for MSB right 
		temp2=int(temp2,2)
		one>>shift
		temp2<<(32-shift)
		one=one+temp2
	else :
		one=LSL(string)
	return one	

"""
this ROR function is string[-25]==0
so, this function is like LSR,LSL
"""
def ROR(string):				# this is ROR function is like C_ROR   
	one=int(string[28:],2)			# this function unlike C_ROR, use register value 
	one=r[one]
        if string[27]=="0":
		shift=int(string[20:25],2)
        	shift=shift*2
	else :
		temp=int(string[20:24],2)
		shift=r[temp]
        tail=one>>shift
        head=(one-(tail<<shift))<<(32-shift)
        return head+tail                        #return value is int
"""
shift operator end, input parameter = string , return int value 
"""
def MOV(bj):
	a=int(bj[16:20],2)			# a is register number
						# b is number
	if bj[-25]=="1":			
		value=C_ROR(bj)
	else:
		if bj[25:27]=="00":
			value=LSL(bj)
		elif bj[25:27]=="01":
			value=LSR(bj)
		elif bj[25:27]=="10":
			value=ASR(bj)
		elif bj[25:27]=="11":
			value=ROR(bj)
	r[a]=value

def ADD(bj):				# ADD function is like data processing MOV 
					# unlike thing is ADD + operator
	a=int(bj[16:20],2)
	op1=int(bj[12:15],2)
	if bj[-25]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value+op1

def SUB(bj):					# SUB function is like data processing like MOV
						# unlike thing is - operator	
        a=int(bj[16:20],2)
        op1=int(bj[12:15],2)
        if bj[-25]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=op1-value

def RSB(bj):

        a=int(bj[16:20],2)
        op1=int(bj[12:15],2)
        if bj[-25]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value - op1


				
"""
this is SWI function
swi is 27~24 is binary 1111

"""
def SWI(bj,address):
	for i in range(15):
		print "r%d"%i," :  ","%d"%r[i]
	print "r15 pc: ", "%x"%(address+8)


lines=sys.stdin.readlines()
Start_Point=lines[0]
memory=dict()

for l in lines[1:]:
      	[addr,value]=l.split(':')
	value.strip()
	addr.strip()
	addr=int(addr,16)
	value=int(value,16)
	
	bj=binaryPrint(value)
	
	if bj[7:11]=="1101":
		MOV(bj)
	elif bj[4:8]=="1111":
		SWI(bj,addr)
	elif bj[4:8]=="0100":
		ADD(bj)




