import sys

"""
this is binary transform fuction
input = decimal some
ouput = string binary bi

"""

"""
this is register instance  r13:stack pointer, r14 : lr , r15: pc 
"""
r=[0]*16
CPSR1="1101"
CPSR2="1101"
gt="1100"

lt="1011"

ge="1010"

le="1101"

al="1110"

nv="1111"

def binaryPrint(some):
	A=[0]*32
	list=[0]
	for i in range(32):
        	reg=some%2
		if some/2==0:
			
         		break
        	some=some/2
        	A[31-i]=reg

	bi=""
	for j in range(32):
        	bi=bi+str(A[j])
	return bi
"""
this is MOV operand
input is binary String

"""
def unsigned32(n):
	return n & 0xFFFFFFFFL
	

def C_ROR(string):
	one=int(string[24:],2)
	shift=int(string[20:24],2)
	if shift==0:
		return one	
	shift=shift*2
	tail=one>>shift
	head=(one-(tail<<shift))<<(32-shift)
	return head+tail			#return value is int
	
def LSL(string):
	global r	
	a=int(string[28:],2)			# string consist of binary number		
	one=r[a]				
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift
	else:
		temp=int(string[20:24],2)	# this case is register number shift
		shift=r[temp]	
	one=one<<shift				# excute shift left	
	return one				# return value is shifted int

def LSR(string):
	global r				# string consist of binary number
	a=int(string[28:],2)
	one=r[a]
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift     
	else:
		temp=int(string[20:24],2)	# this case is register number shift	
		shift=r[temp]
	one=one>>shift				# excute shift right
	return one				# return value is int 

#this ASR function is MSB right shifter

def ASR(string):
	global r
	a=int(string[28:],2)
	one=r[a]
	if string[27]=="0":
		shift=int(string[20:25],2)
	else:
		temp=int(string[20:24],2)
		shift=r[temp]
	if unsigned32(one)>=21474836:		# call unsigned32 function if -1 inserted, MSB 1 ~~~
		temp2="11111111111111111111111111111111" # this number is for MSB right 
		temp2=int(temp2,2)
		one=one>>shift
		temp2<<(32-shift)
		one=one+temp2
	else :
		one=LSL(string)
	return one	

"""
this ROR function is string[-25]==0
so, this function is like LSR,LSL
"""
def ROR(string):				# this is ROR function is like C_ROR   
	global r
	one=int(string[28:],2)			# this function unlike C_ROR, use register value 
	one=r[one]
        if string[27]=="0":
		shift=int(string[20:25],2)
        	shift=shift*2
	else :
		temp=int(string[20:24],2)
		shift=r[temp]
        tail=one>>shift
        head=(one-(tail<<shift))<<(32-shift)
        return head+tail                        #return value is int
"""
shift operator end, input parameter = string , return int value 
"""
def MOV(bj):
	global r
	a=int(bj[16:20],2)			# a is register number
					# b is number
	if bj[6]=="1":			
		value=C_ROR(bj)
	else:
		if bj[25:27]=="00":
			value=LSL(bj)
		elif bj[25:27]=="01":
			value=LSR(bj)
		elif bj[25:27]=="10":
			value=ASR(bj)
		elif bj[25:27]=="11":
			value=ROR(bj)	
	r[a]=value
	

def ADD(bj):				# ADD function is like data processing MOV 
	global r					# unlike thing is ADD + operator
	a=int(bj[16:20],2)
	op1=int(bj[12:16],2)
	if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value+r[op1]
	
def SUB(bj):					# SUB function is like data processing like MOV
	global r					# unlike thing is - operator	
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=r[op1]-value
	
def RSB(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value-r[op1]
	
def AND(bj):	
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        
	r[a]=r[op1]&value
	
def ORR(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=r[op1]|value
	
def EOR(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=r[op1]^value
	
def BIC(bj):
	global r        
	a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=~(r[op1]&value)
	
def MVN(bj):
	global r
        a=int(bj[16:20],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=~value
	

	
"""
this is SWI function
swi is 27~24 is binary 1111

"""
def SWI(bj,address):
	global r
	for i in range(15):
		print "r%d"%i," :  ","%20d"%r[i]
	print "r15 pc: ", "%x"%(address+8)
	
	
def Branch(bj,addr):
	global r							# push,po	
	temp="111111111111111111111111"			# if Branch link bit == 1, r[15]=pc
	temp=int(temp,2)
	offset=int(bj[8:],2)
	if bj[8]=="1":	
		offset=-(offset^temp)
	offset=offset*4+4
	addr=addr+offset
	r[15]=addr+8	
	return addr

def CMP(bj):
	global CPSR1
	global CPSR2
        op1=int(bj[12:16],2)
        if bj[-25]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
       
	res=r[op1]-value
	
	if res<0:
		CPSR1="0001"		
		
		CPSR2="1011"
	elif res==0:
	     	CPSR1="0000"
	elif res>0:
		CPSR1="0001"
		CPSR2="1100"
	

lines=sys.stdin.readlines()
Start_Point=lines[0]
memory=dict()
i=0
main_address=0
first=0
for l in lines[1:]:
      	[addr,value]=l.split(':')				
	value.strip()
	addr.strip()
	addr=int(addr,16)
	value=int(value,16)
	bj=binaryPrint(value)
	print bj		
	if value==int("e1a0f00e",16):
		main_address=addr+4
		r[14]=addr+4
		
	memory[addr]=value	
	if i==0:
		first=addr
		main_address=addr	
		r[14]=addr			# r14 is return address, starting address is stored
			
	i+=1
last_opcode=first+4*(i-1)
excute_address=main_address
r[15]=main_address+8
skip=0
"""
print r[15]

code=memory[excute_address]
bj=binaryPrint(code)
print last_opcode,r[15],main_address,r[14],bj,code
for k in memory.keys():
	print k
"""
COND=""
print "Last Address: %d"%last_opcode,",Main address :%d"%main_address
while 1:
	if excute_address>=last_opcode:
		break 
	code=memory[excute_address]
						# programcounter is excuted operand
	bj=binaryPrint(code)
	
	COND+=bj[0]
	COND+=bj[1]
	COND+=bj[2]
	COND+=bj[3]

	if COND[0]=="0":
		if COND[0:]==CPSR1[0:]:
			skip=0
	elif COND[0]=="1":
		if COND[0:]==al[0:]:                        			
			skip=0

		elif COND[0:]!=CPSR2[0:]:
			skip=1
	if COND[0]==1:
		if COND[0:]==al[0:]:
                        skip=0
		elif COND[0:]==nv[0:]:
			skip=1
		elif COND[0:]==CPSR2:						
			skip=0
		elif COND[0:]==ge[0:]:
			if COND[0:]==gt[0:]:
				skip=0
			elif COND[0:]==eq[0:]:
				skip=0
			elif COND[0:]!=gt[0:]:
				skip=1
		elif COND[0:]==le[0:]:
			if COND[0:]==lt[0:]:			
				skip=0
			elif COND[0:]==eq[0:]:
				skip=0
			elif COND[0:]!=lt[0:]:
				skip=1

	if skip==1:
		r[15]+=4
		excute_address=r[15]-8
		continue
	elif skip==0:
		if bj[4:7]=="101":		
			print excute_address
			if bj[7]=="1":
				r[14]=excute_address+4			
			excute_address=Branch(bj,excute_address)		
			print r[14],excute_address		
			continue		
		elif bj[4:8]=="1111":
			SWI(bj,excute_address)
			r[15]+=4
			excute_address=r[15]-8
			continue
		elif bj[7:11]=="1101":		
			MOV(bj)
			if int(bj[16:20],2)==15:
				excute_address=r[14]
				print "meet Mov pc lr ",r[14],excute_address
				r[15]+=r[14]+8
				r[14]=main_address
				continue	
		elif bj[7:11]=="0100":
			ADD(bj)
		elif bj[7:11]=="0000":
			AND(bj)
		elif bj[7:11]=="0001":
		        EOR(bj)
		elif bj[7:11]=="0010":
		        SUB(bj)
		elif bj[7:11]=="0011":
		        RSB(bj)
		elif bj[7:11]=="0100":
		        ADD(bj)
		elif bj[7:11]=="1100":
		       	ORR(bj)
		elif bj[7:11]=="1110":
		        BIC(bj)
		elif bj[7:11]=="1111":
		        MVN(bj)
		r[15]+=4
		excute_address=r[15]-8
