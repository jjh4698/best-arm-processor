import sys

"""
this is binary transform fuction
input = decimal some
ouput = string binary bi

"""

"""
this is register instance  r13:stack pointer, r14 : lr , r15: pc 
"""
r=[0]*16
CPSR1="1110"
CPSR2="1110"
gt="1100"
lt="1011"
ge="1010"
le="1101"
al="1110"
nv="1111"
eq="0000"
ldr_num=0
str_num=0

def binaryPrint(some):
	A=[0]*32
	list=[0]
	for i in range(32):
        	reg=some%2
		if some/2==0:
			A[31-i]=reg
         		break
        	some=some/2
        	A[31-i]=reg

	bi=""
	for j in range(32):
        	bi=bi+str(A[j])
	return bi
"""
this is MOV operand
input is binary String

"""
def unsigned32(n):
	return n & 0xFFFFFFFFL
	

def C_ROR(string):	
	one=int(string[24:],2)
	shift=int(string[20:24],2)
	if shift==0:
		return one	
	shift=shift*2
	tail=one>>shift
	head=(one-(tail<<shift))<<(32-shift)
	return head+tail			#return value is int
	
def LSL(string):
	global r	
	a=int(string[28:],2)			# string consist of binary number		
	one=r[a]				
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift
	else:
		temp=int(string[20:24],2)	# this case is register number shift
		shift=r[temp]	
	one=one<<shift				# excute shift left	
	

	return one				# return value is shifted int

def LSR(string):
	global r				# string consist of binary number
	a=int(string[28:],2)
	one=r[a]
	if string[27]=="0":			# this case is regi,constant to shift number
		shift=int(string[20:25],2)	# this case is constant shift     
	else:
		temp=int(string[20:24],2)	# this case is register number shift	
		shift=r[temp]
	one=one>>shift				# excute shift right
	return one				# return value is int 

#this ASR function is MSB right shifter

def ASR(string):
	global r
	a=int(string[28:],2)
	one=r[a]
	if string[27]=="0":
		shift=int(string[20:25],2)
	else:
		temp=int(string[20:24],2)
		shift=r[temp]
	if unsigned32(one)>=21474836:		# call unsigned32 function if -1 inserted, MSB 1 ~~~
		temp2="11111111111111111111111111111111" # this number is for MSB right 
		temp2=int(temp2,2)
		one=one>>shift
		temp2<<(32-shift)
		one=one+temp2
	else :
		one=LSL(string)
	return one	

"""
this ROR function is string[-25]==0
so, this function is like LSR,LSL
"""
def ROR(string):				# this is ROR function is like C_ROR   
	global r
	one=int(string[28:],2)			# this function unlike C_ROR, use register value 
	one=r[one]
        if string[27]=="0":
		shift=int(string[20:25],2)
        	shift=shift*2
	else :
		temp=int(string[20:24],2)
		shift=r[temp]
        tail=one>>shift
        head=(one-(tail<<shift))<<(32-shift)
        return head+tail                        #return value is int
"""
shift operator end, input parameter = string , return int value 
"""
def MOV(bj):
	global r
	a=int(bj[16:20],2)			# a is register number
					# b is number
	if bj[6]=="1":			
		value=C_ROR(bj)
		
	else:
		if bj[25:27]=="00":
			value=LSL(bj)
		elif bj[25:27]=="01":
			value=LSR(bj)
		elif bj[25:27]=="10":
			value=ASR(bj)
		elif bj[25:27]=="11":
			value=ROR(bj)
		
	r[a]=value
	

def ADD(bj):				# ADD function is like data processing MOV 
	global r					# unlike thing is ADD + operator
	a=int(bj[16:20],2)
	op1=int(bj[12:16],2)
	if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value+r[op1]
	if bj[11]=="1":
		ADDS(bj)
	print r[a]
def SUB(bj):					# SUB function is like data processing like MOV
	global r					# unlike thing is - operator	
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=r[op1]-value
	if bj[11]=="1":
		CMP(bj)
	
def RSB(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        r[a]=value-r[op1]
	if bj[11]=="1":
		RSBS(bj)
	
def AND(bj):	
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
        
	r[a]=r[op1]&value
	
def ORR(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=r[op1]|value
	
def EOR(bj):
	global r
        a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=r[op1]^value
	
def BIC(bj):
	global r        
	a=int(bj[16:20],2)
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=~(r[op1]&value)
	
def MVN(bj):
	global r
        a=int(bj[16:20],2)
        if bj[6]=="1":
                value=C_ROR(bj)
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
                elif bj[25:27]=="01":
                        value=LSR(bj)
                elif bj[25:27]=="10":
                        value=ASR(bj)
                elif bj[25:27]=="11":
                        value=ROR(bj)
	r[a]=~value
	

	
"""
this is SWI function
swi is 27~24 is binary 1111

"""
def SWI(bj,address):
	global r
	for i in range(15):
		print "r%d"%i," :  ","%20d"%r[i]
	print "r15 pc: ", "%x"%(address+8)
	
	
def Branch(bj,addr):
	global r							# push,po	
	temp="111111111111111111111111"			# if Branch link bit == 1, r[15]=pc
	temp=int(temp,2)
	offset=int(bj[8:],2)
	if bj[8]=="1":	
		offset=-(offset^temp)
	offset=offset*4+4
	addr=addr+offset
	r[15]=addr+8	
	return addr

def ADDS(bj):
	global CPSR1
	global CPSR2
	
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
		print "C_ROR"
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
			
                elif bj[25:27]=="01":
                        value=LSR(bj)
			
                elif bj[25:27]=="10":
                        value=ASR(bj)
			
                elif bj[25:27]=="11":
                        value=ROR(bj)
			
        
	res=r[op1]+value
	print "r0,10",r[op1],value	
	if res<0:
		CPSR1="0001"				
		CPSR2="1011"			#LT
	elif res==0:
	     	CPSR1="0000"
		CPSR2="4444"
	elif res>0:
		CPSR1="0001"
		CPSR2="1100"			#GT
	
def RSBS(bj):
	global CPSR1
	global CPSR2
	
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
		print "C_ROR"
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
			
                elif bj[25:27]=="01":
                        value=LSR(bj)
			
                elif bj[25:27]=="10":
                        value=ASR(bj)
			
                elif bj[25:27]=="11":
                        value=ROR(bj)
			
        
	res=value-r[op1]
	print "r0,10",r[op1],value	
	if res<0:
		CPSR1="0001"				
		CPSR2="1011"			#LT
	elif res==0:
	     	CPSR1="0000"
		CPSR2="4444"
	elif res>0:
		CPSR1="0001"
		CPSR2="1100"			#GT
	
def LDR_STR(bj,excute_address):
	global r
	global ldr_num,str_num
	String_code=""
	base_register=int(bj[12:16],2)
	a=int(bj[16:20],2)
	Ldr_Address=0
	String=""
	if bj[6]=="0":				# REGISTER OFFSET, CONSTANT OFFSET
		value=int(bj[20:],2)	
	else:
		shift=int(bj[20:28],2)
		regi=int(bj[28:],2)
		value=r[regi]		
		value=value<<shift
	if bj[8]=="0":				# UP,DOWN bit
		value=-value
	if bj[7]=="0":				# PRE,POST bit
		Ldr_Address=r[base_register]+value
		r[base_register]+=value		# base register plus	
	else:
		Ldr_Address=r[base_register ]+value
	if bj[10]=="1":				# WRITE,BACK bit
		r[base_register]+=value		# base register plus
	if bj[11]=="1":				# LOAD bit
		if bj[9]=="0":
			code=memory[Ldr_Address]
			string_code=binaryPrint(code)
			r[a]=int(string_code,2)
		elif bj[9]=="1":
			code=memory[Ldr_Address]
			string_code=binaryPrint(code)
			if ldr_num==0:		
				r[a]=int(string_code[0:8],2)
				ldr_num+=1
			elif ldr_num==1:
				r[a]=int(string_code[8:16],2)
				ldr_num+=1
			elif ldr_num==2:
				r[a]=int(string_code[16:24],2)
				ldr_num+=1				
			elif ldr_num==3:
				r[a]=int(string_code[24:32],2)
				ldr_num+=1
	else:					# STORE bit
		if bj[9]=="0":
			memory[Ldr_Address]=r[a]
		elif bj[9]=="1":
			String=binaryPrint(r[a])
			if str_num==0:		
				temp=int(String[0:8],2)
				str_num+=1
			elif str_num==1:
				temp=int(String[8:16],2)
				str_num+=1
			elif str_num==2:
				temp=int(String[16:24],2)
				str_num+=1				
			elif str_num==3:
				temp=int(String[24:32],2)
				str_num+=1
			
			memory[Ldr_Address]=temp		
		print "Store bit on"
def LDM_STM(bj):
	global r
	base_register_num=int(bj[12:16],2)
	base_register=r[base_register_num]
	base_register_back=r[base_register_num]
	i=0	
	j=0
	choice=[0]*16
	for i in range(16):
		
		if bj[i+16]=="1":
			choice[j]=15-i
			j=j+1
		if i==15:		
			choice[j]=-1		# -1 is last number

	if bj[11]=="0":			# store
		if bj[7]=="0":		# post
			if bj[8]=="0":	# store - post - down
				for j in range(16):				
					if choice[j]==-1:
						break
					memory[base_register]=r[choice[j]]
					base_register-=4
							
			elif bj[8]=="1":# store - post - up
				for j in range(16):				
					if choice[j]==-1:
						break
					memory[base_register]=r[choice[j]]
					base_register+=4
		elif bj[7]=="1":	# pre
			if bj[8]=="0":	# store - pre - down
				for j in range(16):				
					if choice[j]==-1:
						break
					base_register-=4					
					memory[base_register]=r[choice[j]]
					
			elif bj[8]=="1":	# stpre - pre - up
				for j in range(16):				
					if choice[j]=="-1":
						break
					base_register+=4					
					memory[base_register]=r[choice[j]]
	elif bj[11]=="1":		# load
		if bj[7]=="0":		# post
			if bj[8]=="0":	# load - post - down
				for j in range(16):				
					if choice[j]==-1:
						break					
					r[choice[j]]=memory[base_register]
					base_register-=4
			elif bj[8]=="1":# load - post - up
				for j in range(16):					
					if choice[j]==-1:
						break					
					r[choice[j]]=memory[base_register]
					base_register+=4
		elif bj[7]=="1":	# pre
			if bj[8]=="0":	# load - pre - down
				for j in range(16):					
					if choice[j]==-1:
						break					
					base_register-=4					
					r[choice[j]]=memory[base_register]
					
			elif bj[8]=="1":	# load - pre - up
				for j in range(16):					
					if choice[j]==-1:
						break					
					base_register+=4					
					r[choice[j]]=memory[base_register]
	if bj[10]=="0":
		r[base_register_num]=base_register_back			# no write back
	elif bj[10]=="1":
		r[base_register_num]=base_register			# yes write backw
	
def CMP(bj):
	global CPSR1
	global CPSR2
	
        op1=int(bj[12:16],2)
        if bj[6]=="1":
                value=C_ROR(bj)
		print "C_ROR"
        else:
                if bj[25:27]=="00":
                        value=LSL(bj)
			
                elif bj[25:27]=="01":
                        value=LSR(bj)
			
                elif bj[25:27]=="10":
                        value=ASR(bj)
			
                elif bj[25:27]=="11":
                        value=ROR(bj)
			
        
	res=r[op1]-value
	print "r0,10",r[op1],value	
	if res<0:
		CPSR1="0001"				
		CPSR2="1011"			#LT
	elif res==0:
	     	CPSR1="0000"
		CPSR2="4444"
	elif res>0:
		CPSR1="0001"
		CPSR2="1100"			#GT
	

lines=sys.stdin.readlines()
Start_Point=lines[0]
memory=dict()
i=0
main_address=0
first=0
ex_addr=0
data_address=0
for l in lines[1:]:
      	[addr,value]=l.split(':')				
	value.strip()
	addr.strip()
	addr=int(addr,16)
			# to know .data section
	if addr-ex_addr>200:
		data_address=addr
		last_mainaddress=ex_addr	
	ex_addr=addr	
	value=int(value,16)
	bj=binaryPrint(value)
	print bj		
	if value==int("e1a0f00e",16):
		main_address=addr+4
		r[14]=addr+4
		
	memory[addr]=value	
	if i==0:
		first=addr
		main_address=addr	
		r[14]=addr			# r14 is return address, starting address is stored
			
	i+=1

last_opcode=addr
excute_address=main_address
r[15]=main_address+8
skip=0
j=0
"""print r[15]

code=memory[excute_address]
bj=binaryPrint(code)
print last_opcode,r[15],main_address,r[14],bj,code
for k in memory.keys():
	print k
"""
j=1
k=0					#extern value counter.
COND=""
data=0					# data section,  0 is off , 1 is on
extern_value=[0]*30
print "Last Address: %d"%last_opcode,",Main address :%d"%main_address
while 1:
	print excute_address,last_mainaddress,data_address
	if excute_address>=last_mainaddress and j==1:
		excute_address=data_address
		r[15]=data_address+8
		j=0
		data=1
	if excute_address>=last_opcode+4:
		print "meet break"	
		break 
	code=memory[excute_address]
	print "now playing : ",excute_address					# programcounter is excuted operand

	bj=binaryPrint(code)
	if data==1:			# if data section is on,
		extern_value[k]=int(bj[0:],2)
		print "data section",extern_value[k]
		k+=1
	COND=""
	COND+=bj[0:4]

	print COND,CPSR2
	if COND[0]=='0':
		if COND[0:]==CPSR1[0:]:
			skip=0		
		elif COND[0:]!=CPSR1[0:]:
			skip=1
	elif COND[0]=='1':
		if COND[0:]==al:
                        skip=0
			print "al"
		elif COND[0:]==nv:
			skip=1
			print "nv"
		elif COND[0:]==CPSR2:						
			skip=0
			print "==CPSR2"	
		elif COND[0:]==ge:
			if CPSR2[0:]==gt:
				skip=0
			else: 
				skip=1
		elif COND[0:]==le:
			if CPSR2[0:]==lt:
				skip=0
			else:
				skip=1
			print "le"
		else:
			skip=1
			print "else"

	if skip==1:	
		r[15]+=4
		excute_address=r[15]-8
		continue
	elif skip==0:
		if bj[4:7]=="101":		
			print excute_address
			if bj[7]=="1":
				r[14]=excute_address+4			
			excute_address=Branch(bj,excute_address)		
			print r[14],excute_address		
			continue		
		elif bj[4:8]=="1111":
			SWI(bj,excute_address)
			r[15]+=4
			excute_address=r[15]-8
			continue
		elif bj[4:6]=="01":
			LDR_STR(bj,excute_address)
			r[15]+=4
			excute_address=r[15]-8
			continue
		elif bj[4:7]=="100":
			LDM_STM(bj)
		elif bj[7:11]=="1101":		
			MOV(bj)
			if int(bj[16:20],2)==15:
				excute_address=r[14]
				print "meet Mov pc lr ",r[14],excute_address
				r[15]=r[14]+8
				r[14]=main_address
				continue	
		elif bj[7:11]=="0100":
			ADD(bj)
		elif bj[7:11]=="0000":
			AND(bj)
		elif bj[7:11]=="0001":
		        EOR(bj)
		elif bj[7:11]=="0010":
		        SUB(bj)
		elif bj[7:11]=="0011":
		        RSB(bj)
		elif bj[7:11]=="0100":
		        ADD(bj)
		elif bj[7:11]=="1100":
		       	ORR(bj)
		elif bj[7:11]=="1110":
		        BIC(bj)
		elif bj[7:11]=="1111":
		        MVN(bj)
		elif bj[7:11]=="1010":
			CMP(bj)	
		r[15]+=4
		excute_address=r[15]-8
print memory[69668]
